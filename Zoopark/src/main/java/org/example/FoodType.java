package org.example;

public enum FoodType {
	GRASS,
	BEEF,
	FISH
}
