package org.example;

public class Camel extends Animals implements AnimalsMethods {
	FoodType foodType = FoodType.GRASS;
	AnimalType animalType = AnimalType.GROUND;
	AnimalType2 animalType2 = AnimalType2.HERBIVORE;
	String name = "Верблюд";

	@Override
	public void move() {
		animalMove(name, animalType);
	}

	@Override
	public void eat(FoodType food) {
		animalEat(name, foodType, food);
	}


}