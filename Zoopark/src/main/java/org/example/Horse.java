package org.example;

public class Horse extends Animals implements AnimalsMethods {
	FoodType foodType = FoodType.GRASS;
	AnimalType animalType = AnimalType.GROUND;
	AnimalType2 animalType2 = AnimalType2.HERBIVORE;
	String name = "Лошадь";

	@Override
	public void move() {
		animalMove(name, animalType);
	}

	@Override
	public void eat(FoodType food) {
		animalEat(name, foodType, food);
	}
}
