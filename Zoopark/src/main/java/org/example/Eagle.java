package org.example;

public class Eagle extends Animals implements AnimalsMethods {
	FoodType foodType = FoodType.BEEF;
	AnimalType animalType = AnimalType.AIR;
	AnimalType2 animalType2 = AnimalType2.PREDATOR;
	String name = "Орел";

	@Override
	public void move() {
		animalMove(name, animalType);
	}

	@Override
	public void eat(FoodType food) {
		animalEat(name, foodType, food);
	}


}