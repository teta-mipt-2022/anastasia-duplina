package org.example;

public enum AnimalType {
    GROUND,
    AIR,
    WATER
}
