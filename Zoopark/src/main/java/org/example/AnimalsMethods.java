package org.example;

public interface AnimalsMethods {
	void move();
	void eat(FoodType food);

}
