package org.example;

public class Dolfin extends Animals implements AnimalsMethods {
	FoodType foodType = FoodType.FISH;
	AnimalType animalType = AnimalType.WATER;
	AnimalType2 animalType2 = AnimalType2.PREDATOR;
	String name = "Дельфин";

	@Override
	public void move() {
		animalMove(name, animalType);
	}

	@Override
	public void eat(FoodType food) {
		animalEat(name, foodType, food);
	}


}