package org.example;

public abstract class Animals {

	void animalMove(String name, AnimalType animalType) {
		switch (animalType) {
			case AIR -> System.out.println(name + " летит");
			case WATER -> System.out.println(name + " плывет");
			case GROUND -> System.out.println(name + " ходит");
		}
	}

	void animalEat(String name, FoodType foodType, FoodType food) {
		if (food == foodType) {
			System.out.println(name + " ест");
		} else {
			System.out.println(name + " не ест");
		}
	}
}
