package org.example;

public class Tiger extends Animals implements AnimalsMethods {
	FoodType foodType = FoodType.BEEF;
	AnimalType animalType = AnimalType.GROUND;
	AnimalType2 animalType2 = AnimalType2.PREDATOR;
	String name = "Тигр";

	@Override
	public void move() {
		animalMove(name, animalType);
	}

	@Override
	public void eat(FoodType food) {
		animalEat(name, foodType, food);
	}

}