package org.example;

public class Main {
	public static void main(String[] args) {
		Horse horse = new Horse();
		horse.move();
		horse.eat(FoodType.GRASS);
		Eagle eagle = new Eagle();
		eagle.move();
		eagle.eat(FoodType.BEEF);
		Camel camel = new Camel();
		camel.eat(FoodType.GRASS);
		camel.move();
		Tiger tiger =new Tiger();
		tiger.move();
		tiger.eat(FoodType.FISH);
		Dolfin dolfin = new Dolfin();
		dolfin.eat(FoodType.FISH);
		dolfin.move();
	}
}
