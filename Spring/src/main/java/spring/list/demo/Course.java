package spring.list.demo;

import lombok.Getter;
import lombok.Setter;
import lombok.With;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;

@With
@Getter
@Setter
public class Course {

	Long id;
	@NotBlank(message = "Course author has to be filled")
	private String author;
	@NotBlank(message = "Course title has to be filled")
	private String title;

	public Course(long id, String author, String title) {
		this.id = id;
		this.author = author;
		this.title = title;
	}

	public Course(String author, String title) {
		id=null;
		this.author = author;
		this.title = title;
	}

	public Course(Course course) {
		this.id = course.id;
		this.author = course.author;
		this.title = course.title;
	}


}
