package spring.list.demo;

import java.util.List;
public interface CourseRepository {
	Course findById(Long id);

	List<Course> findAll();

	Course save(Course course) ;

	void deleteById(Long id);

	List<Course> findByTitleWithPrefix(String prefix);
}