package spring.list.demo;

import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Component
public class InMemoryCourseRepository implements CourseRepository {
	private Map<Long, Course> courseMap;
	private final AtomicLong idGenerator = new AtomicLong(0);

	public InMemoryCourseRepository() {
		courseMap = new ConcurrentHashMap<>();
		long id=idGenerator.incrementAndGet();
		courseMap.put(id,new Course( id,"Петров А.В.", "Основы кройки и шитья"));
		id=idGenerator.incrementAndGet();
		courseMap.put(id,new Course(id, "Мошкина А.В", "Введение в архитектурный дизайн"));
	}

	@Override
	public Course findById(Long id){
		if (courseMap.containsKey(id)) {
			return courseMap.get(id);
		}else throw new NoSuchCourseException("No course with id=" + id,OffsetDateTime.now());


	}

	@Override
	public List<Course> findAll() {
		return new ArrayList<>(courseMap.values());
	}

	@Override
	public Course save(Course course){
		if (course.getId() == null) {
			course.setId(idGenerator.incrementAndGet());
			courseMap.put(course.id, course);
			return new Course(course);
		}
		else {
				if(courseMap.containsKey(course.id)){
					courseMap.put(course.id, course);
					return new Course(course);
				}else throw new NoSuchCourseException("No course with id=" + course.getId(),OffsetDateTime.now());
		}
	}

	@Override
	public void deleteById(Long id){
		if (courseMap.containsKey(id)) {
			courseMap.remove(id);
		}else throw new NoSuchCourseException("No course with id=" + id,OffsetDateTime.now());

	}
	@Override
	public List<Course> findByTitleWithPrefix(String prefix) {
		return courseMap.values()
				.stream().filter(course -> course.getTitle().startsWith(prefix))
				.collect(Collectors.toList());
	}
}