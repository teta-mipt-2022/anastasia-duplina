package spring.list.demo.controllers;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import spring.list.demo.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/courses")
@Validated
@AllArgsConstructor
public class CourseController {
	private final Service service;

	@GetMapping("")
	public List<Course> courseTable() {
		return service.findAll();
	}
	@GetMapping("/{id}")
	public Course getCourse(@PathVariable("id") Long id) {
		return service.findById(id);
	}
	@PutMapping("/{id}")
	public void updateCourse(@PathVariable Long id,
	                         @Valid @RequestBody CourseRequest request){
		service.updateCourse(id,request);
	}
	@PostMapping
	public Course createCourse(@Valid @RequestBody CourseRequest request){
		return service.createCourse(request);
	}
	@DeleteMapping("/{id}")
	public void deleteCourse(@PathVariable Long id) {
		service.deleteCourse(id);
	}
	@GetMapping("/filter")
	public List<Course> getCoursesByTitlePrefix(@RequestParam(name = "titlePrefix", required = false) String titlePrefix) {
		return service.getCoursesByTitlePrefix(titlePrefix);

	}

}
