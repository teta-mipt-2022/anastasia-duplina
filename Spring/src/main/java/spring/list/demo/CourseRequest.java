package spring.list.demo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
@Data
public class CourseRequest {
	@NotBlank(message = "Course author has to be filled")
	private String author;
	@NotBlank(message = "Course title has to be filled")
	private String title;

}