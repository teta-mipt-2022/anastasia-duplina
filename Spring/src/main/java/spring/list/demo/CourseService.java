package spring.list.demo;

import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.Objects.requireNonNullElse;

@Component
public class CourseService implements Service {
	private final CourseRepository courseRepository;

	public CourseService(CourseRepository courseRepository) {
		this.courseRepository = courseRepository;
	}

	@Override
	public List<Course> findAll() {
		return courseRepository.findAll();
	}

	@Override
	public Course findById(Long id) throws NoSuchCourseException {
		return courseRepository.findById(id);
	}

	@Override
	public void updateCourse(Long id, CourseRequest request) {
		Course course = courseRepository.findById(id);
		course.setTitle(request.getTitle());
		course.setAuthor(request.getAuthor());
		courseRepository.save(course);
	}

	@Override
	public Course createCourse(CourseRequest request) {
		Course course = new Course(request.getAuthor(), request.getTitle());
		return courseRepository.save(course);
	}

	@Override
	public void deleteCourse(Long id) {
		courseRepository.deleteById(id);
	}

	@Override
	public List<Course> getCoursesByTitlePrefix(String titlePrefix) {
		return courseRepository.findByTitleWithPrefix(requireNonNullElse(titlePrefix, ""));
	}
}
