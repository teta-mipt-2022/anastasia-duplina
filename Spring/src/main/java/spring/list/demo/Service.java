package spring.list.demo;

import java.util.List;

public interface Service {
	List<Course> findAll();
	Course findById(Long id);
	void updateCourse(Long id, CourseRequest request);
	Course createCourse( CourseRequest request);
	void deleteCourse( Long id);
	List<Course> getCoursesByTitlePrefix(String titlePrefix);

}
