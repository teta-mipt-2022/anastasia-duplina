package spring.list.demo;

import lombok.Getter;

import java.time.OffsetDateTime;
@Getter
public class NoSuchCourseException extends RuntimeException{
	String message;
	OffsetDateTime dateOccurred;
	public NoSuchCourseException(String message, OffsetDateTime dateOccurred){
		this.message=message;
		this.dateOccurred=dateOccurred;
	}
}