package org.example;

import org.junit.Test;

import java.util.concurrent.*;

import static org.junit.jupiter.api.Assertions.*;

public class EnrichmentServiceTest {
	@Test
	public void end2end() throws ExecutionException, InterruptedException {
		NamesAndMsisdns map = new NamesAndMsisdns();
		map.addPerson("88005553531", new Person("Vasya1", "Ivanov1"));
		map.addPerson("88005553532", new Person("Vasya2", "Ivanov2"));
		map.addPerson("88005553533", new Person("Vasya3", "Ivanov3"));
		map.addPerson("88005553534", new Person("Vasya4", "Ivanov4"));
		map.addPerson("88005553535", new Person("Vasya5", "Ivanov5"));
		String exampleJSON1 = "{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"88005553532\"}";
		EnrichmentService enrichmentService = new EnrichmentService();
		Message message1 = new Message();
		message1.setContent(exampleJSON1);
		message1.setEnrichmentType(Message.EnrichmentType.MSISDN);
		String exampleJSON2 = "{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"88005553535\",\"enrichment\":{\"firstname\":\"Vasya2\",\"lastname\":\"Ivanov2\"}}";
		Message message2 = new Message();
		message2.setContent(exampleJSON2);
		message2.setEnrichmentType(Message.EnrichmentType.MSISDN);
		String exampleJSON3 = "{\"action\":\"button_click\",\"page\":\"book_card\"}";
		Message message3 = new Message();
		message3.setContent(exampleJSON3);
		message3.setEnrichmentType(Message.EnrichmentType.MSISDN);
		String exampleJSON4 = "{";
		Message message4 = new Message();
		message4.setContent(exampleJSON4);
		message4.setEnrichmentType(Message.EnrichmentType.MSISDN);
		ExecutorService executor = Executors.newFixedThreadPool(10);
		Future<String> future1 = executor.submit(() -> enrichmentService.enrich(message1));
		Future<String> future2 = executor.submit(() -> enrichmentService.enrich(message2));
		Future<String> future3 = executor.submit(() -> enrichmentService.enrich(message3));
		Future<String> future4 = executor.submit(() -> enrichmentService.enrich(message4));
		String response1 = future1.get();
		String response2 = future2.get();
		String response3 = future3.get();
		String response4 = future4.get();
		String exampleJSONresponce1 = "{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"88005553532\",\"enrichment\":{\"firstname\":\"Vasya2\",\"lastname\":\"Ivanov2\"}}";
		String exampleJSONresponce2 = "{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"88005553535\",\"enrichment\":{\"firstname\":\"Vasya5\",\"lastname\":\"Ivanov5\"}}";
		String exampleJSONresponce3 = "{\"action\":\"button_click\",\"page\":\"book_card\"}";
		String exampleJSONresponce4 = "{";
		assertEquals(exampleJSONresponce1, response1);
		assertEquals(exampleJSONresponce2, response2);
		assertEquals(exampleJSONresponce3, response3);
		assertEquals(exampleJSONresponce4, response4);
	}

}