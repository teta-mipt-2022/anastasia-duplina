package org.example;

import java.util.concurrent.ConcurrentHashMap;

public class Main {
	public static void main(String[] args) {
		NamesAndMsisdns map = new NamesAndMsisdns();
		ConcurrentHashMap<String, Person> msisdn = new ConcurrentHashMap<>();
		map.addPerson("88005553531", new Person("Vasya1", "Ivanov1"));
		map.addPerson("88005553532", new Person("Vasya2", "Ivanov2"));
		map.addPerson("88005553533", new Person("Vasya3", "Ivanov3"));
		map.addPerson("88005553534", new Person("Vasya4", "Ivanov4"));
		map.addPerson("88005553535", new Person("Vasya5", "Ivanov5"));
		String exampleJSON = "{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"88005553535\",\"enrichment\":{\"firstname\":\"Vasya2\",\"lastname\":\"Ivanov2\"}}";
		EnrichmentService enrichmentService = new EnrichmentService();
		Message message = new Message();
		message.setContent(exampleJSON);
		message.setEnrichmentType(Message.EnrichmentType.MSISDN);
		System.out.println(enrichmentService.enrich(message));

	}
}