package org.example;

import lombok.Data;

import java.util.HashMap;

@Data
public class Person {
	String firstName;
	String lastName;

	public Person(String firstname, String lastname) {
		this.firstName = firstname;
		this.lastName = lastname;
	}

	public HashMap toMap() {
		HashMap<String, String> map = new HashMap<>();
		map.put("firstname", firstName);
		map.put("lastname", lastName);
		return map;
	}
}
