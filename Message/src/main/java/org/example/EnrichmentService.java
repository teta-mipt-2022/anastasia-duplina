package org.example;

import com.google.gson.Gson;
import com.jayway.jsonpath.JsonPath;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class EnrichmentService {
	ConcurrentHashMap<Integer,Message> listOfGoodMessages;
	ConcurrentHashMap<Integer,Message> listOfBadMessages;

	public EnrichmentService() {
		listOfGoodMessages =new ConcurrentHashMap<>();
		listOfBadMessages =new ConcurrentHashMap<>();
	}

	public boolean isJSON(String content) {
		try {
			new JSONObject(content);
		} catch (JSONException e) {
			return false;
		}
		return true;
	}

	public boolean hasMSISDN(String content) {
		try {
			JsonPath.read(content, "$.msisdn");
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public String getMSISDN(String content) {
		String msisdn;
		if (isJSON(content)) {
			if (hasMSISDN(content)) {
				msisdn = JsonPath.read(content, "$.msisdn");
				if (msisdn == null) {
					msisdn = "-1";
				}
			} else {
				msisdn = "-1";
			}
		} else {
			msisdn = "-1";
		}

		return msisdn;
	}

	public void putGoodMessage(Message message) {
		listOfGoodMessages.put(listOfGoodMessages.size(), message);
	}

	public void putBadMessage(Message message) {
		listOfBadMessages.put(listOfBadMessages.size(), message);
	}

	public String enrichByMSISDN(Message message) {
		String msisdn = getMSISDN(message.getContent());
		Person person;
		if (!msisdn.equals("-1") && NamesAndMsisdns.concurrentHashMap.containsKey(msisdn)) {
			person = NamesAndMsisdns.concurrentHashMap.get(msisdn);
			Gson gson = new Gson();
			Map<String, Object> map = gson.fromJson(message.getContent(), Map.class);
			map.put("enrichment", person.toMap());
			String json = gson.toJson(map);
			message.setContent(json);
			putGoodMessage(message);
			return json;
		} else {
			putBadMessage(message);
			return message.getContent();
		}
	}

	public String enrich(Message message) {
		if(message.getEnrichmentType()== Message.EnrichmentType.MSISDN){
			return enrichByMSISDN(message);
		}
		return message.getContent();
	}
}
