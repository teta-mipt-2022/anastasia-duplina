package org.example;

import java.util.concurrent.ConcurrentHashMap;

public class NamesAndMsisdns {
	static public ConcurrentHashMap<String, Person> concurrentHashMap;

	public NamesAndMsisdns() {
		concurrentHashMap = new ConcurrentHashMap<>();
	}
	public void addPerson(String msisdn, Person person){
		concurrentHashMap.put(msisdn,person);
	}
}
