package org.example;


public record CheckRequest(
		String login,
		String request
) {}
