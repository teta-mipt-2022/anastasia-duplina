package org.example;
import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@Slf4j
public class Receiver {
	@Autowired
	private RabbitTemplate rabbitTemplate;
	public Gson gson=new Gson();
	@Autowired
	public RequestRepository requestRepository;



	@RabbitListener(queues = "user", ackMode = "MANUAL")
	public void receive1(String text, MessageHeaders headers, Channel channel,
	                     @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws InterruptedException, IOException {

		CheckRequest checkRequest=gson.fromJson(text,CheckRequest.class);
		Request request=requestRepository.findByTestEquals(checkRequest.request());
		if(request==null){
			request=new Request();
			request.setTest(checkRequest.request());
			requestRepository.save(request);
			log.info("text: " + checkRequest.login() );
		}
		channel.basicAck(tag, false);
		log.info("Acknowledge");
	}

}

