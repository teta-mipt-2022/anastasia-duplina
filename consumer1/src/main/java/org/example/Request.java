package org.example;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="requests")
public class Request {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name="test", unique = true)
	String test;
}

