package org.example;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "client", url = "http://localhost:8083")

public interface Client {
	@PostMapping(value = "/test", consumes = "application/json",produces = "application/json")
	int test(@RequestBody String test);
}
