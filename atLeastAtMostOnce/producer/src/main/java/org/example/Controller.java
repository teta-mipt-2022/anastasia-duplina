package org.example;

import feign.Feign;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.Random;
import java.util.concurrent.*;

@RestController
@RequestMapping("")
@Slf4j
public class Controller {
	@Autowired
	private Client feignClient;
	final Random random = new Random();
//	@Value("${orders.service.url:http://localhost:8083}")
//	private String serviceUrl;
//	@PostConstruct
//	private void configure() {
//		feignClient = Feign.builder()
//				.encoder(new JacksonEncoder())
//				.decoder(new JacksonDecoder())
//				.target(Client.class, serviceUrl);
//	}




	@GetMapping("/start")
	public String start(){
		int t=random.nextInt(100);
		log.info(t+"");
		ExecutorService executor = Executors.newCachedThreadPool();
		Callable<String> task = new Callable<String>() {
			public String call() {

				int s=feignClient.test(String.valueOf(t));
				return "ok "+s;
			}
		};
		String result="";
		int counter=1;
		int n=3;
		while(counter<=n){
			try {
				Future<String> future = executor.submit(task);
				result = future.get(3, TimeUnit.SECONDS);
				break;

			} catch (Exception ex) {
				log.info("fail");
				result="fail";
				counter++;
			}
		}
		log.info("result: "+result);
		return result;
	}
}
