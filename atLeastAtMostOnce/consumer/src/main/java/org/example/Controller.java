package org.example;

import com.sun.source.util.Trees;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Random;
import java.util.TreeSet;

@RestController
@RequestMapping("")
@Slf4j
public class Controller {
	@Autowired
	private RequestRepository requestRepository;
	final Random random=new Random();

	@PostMapping("/test")
	public int test(@RequestBody String test) throws InterruptedException {
		int t=random.nextInt(6)+1;
		log.info("время выполнения: "+t);
		Request request=requestRepository.findByTestEquals(test);
		if(request==null){
			request=new Request();
			request.setTest(test);
			requestRepository.save(request);
			Thread.sleep(t*1000);//рандомное время выполнения
			log.info(test+ "");
		}else{
			Thread.sleep(t*1000);//рандомное время выполнения чего-то еще
		}
		return t;
	}
}
