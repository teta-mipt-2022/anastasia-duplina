package org.example;

import jakarta.persistence.*;
import lombok.Data;
@Data
@Entity
@Table(name="requests")
public class Request {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column( unique = true)
	String test;
}

