package org.example;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("")
@Slf4j
@Validated
public class Controller {
	@Autowired
	private UserService userService;

	@PostMapping("/addUser")
	public void addUser(@RequestBody @Valid AddUser addUser){
		log.info(addUser.toString());
		userService.saveUser(addUser.login(), addUser.password());
	}
}
