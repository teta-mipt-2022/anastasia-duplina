package org.example;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Random;

@Service
@Slf4j
public class UserService {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private OutBoxRepository outBoxRepository;
	@Autowired
	private RabbitTemplate rabbitTemplate;
	public Random random=new Random();
	public Gson gson=new Gson();
	@Transactional
	public void saveUser(String login,String password) {
		log.info("save user:"+login);
		int t=random.nextInt()*1000;
		User user = new User();
		user.setLogin(login);
		user.setPassword(password);
		userRepository.save(user);
		OutBox outBox=new OutBox();
		outBox.setUser_id(user.getId());
		outBox.setLogin(login);
		outBox.setRequest(String.valueOf(t));
		outBoxRepository.save(outBox);
	}
	@Scheduled(fixedDelay = 10000)
	@Transactional
	public void processOutbox() {
		log.info("schedule!");
		List<OutBox> list=outBoxRepository.findAll();
		for (OutBox outbox : list) {
			Request request=new Request(outbox.login,outbox.request);
			rabbitTemplate.convertAndSend("user",gson.toJson(request));
		}
		outBoxRepository.deleteAll(list);
	}

}
