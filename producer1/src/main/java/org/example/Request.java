package org.example;

import lombok.Data;

public record Request(
		String login,
		String request
) {
	public Request(String login,
	               String request) {
		this.login=login;
		this.request=request;
	}
}
