package org.example;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="outbox")
public class OutBox {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column( unique = true)
	Long user_id;
	@Column
	String login;
	@Column
	String request;
}
