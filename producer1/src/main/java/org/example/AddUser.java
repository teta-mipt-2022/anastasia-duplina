package org.example;

import lombok.Data;

import javax.validation.constraints.NotBlank;

public record AddUser(
		@NotBlank
		String login,
		@NotBlank
		String password
) {}
