package spring.list.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;
import spring.list.demo.model.Course;
import spring.list.demo.model.User;
import spring.list.demo.repository.CourseRepository;
import spring.list.demo.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class CourseService {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private CourseRepository courseRepository;


	@Transactional
	public Course assignUserToCourse(Long userId, Long courseId) {
		User user=null;
		Course course=null;
		if(userRepository.findById(userId).isPresent()){
			user = userRepository.findById(userId).get();
		}
		if(courseRepository.findById(courseId).isPresent()){
			course = courseRepository.findById(courseId).get();
		}
		assert course != null;
		course.getUsers().add(user);
		assert user != null;
		user.getCourses().add(course);
		return courseRepository.save(course);
	}

	@Transactional
	public void removeUserFromCourse(Long userId, Long courseId) {
		User user=null;
		Course course=null;
		if(userRepository.findById(userId).isPresent()){
			user = userRepository.findById(userId).get();
		}
		if(courseRepository.findById(courseId).isPresent()){
			course = courseRepository.findById(courseId).get();
		}
		assert user != null;
		user.getCourses().remove(course);
		assert course != null;
		course.getUsers().remove(user);
		courseRepository.save(course);

	}
}
