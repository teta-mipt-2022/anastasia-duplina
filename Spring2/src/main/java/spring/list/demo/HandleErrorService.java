//package spring.list.demo;
//
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.validation.ObjectError;
//import org.springframework.web.bind.MethodArgumentNotValidException;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.RestControllerAdvice;
//import java.time.format.DateTimeFormatter;
//
//@RestControllerAdvice
//public class HandleErrorService {
//
//	@ExceptionHandler
//	public ResponseEntity<String> noSuchCourseExceptionHandler(NoSuchCourseException ex) {
//		return new ResponseEntity<>(ex.getMessage()+" "+ex.getDateOccurred().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME),
//				HttpStatus.NOT_FOUND
//		);
//	}
//
//	@ExceptionHandler
//	public ResponseEntity<String> methodArgumentNotValidException(MethodArgumentNotValidException ex) {
//		return ResponseEntity.badRequest().body(ex.getMessage());
//	}
//
//}