package spring.list.demo.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Date;

@Entity
@Table(name = "lessons")
@Getter
@Setter
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class Lesson {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank(message = "Course title have to be filled")
	@Column
	private String title;


	@Column
	private String description;


	@Column
	private LocalDate date;

	@NotNull
	@Column
	private Long userId;
	@NotNull
	@Column
	private Long couresId;

	@ManyToOne(optional = false)
	private Course course;

}