package spring.list.demo.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "courses")
@Getter
@Setter
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class Course {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank(message = "Course title have to be filled")
	@Column
	private String title;

	@NotNull
	@Column
	private String description;


	@Column
	private LocalDate date;

	@NotNull
	@Column
	private Integer userId;

	@Column
	private Integer rating;


	@ManyToMany
	private Set<User> users;
}