package spring.list.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import spring.list.demo.model.Lesson;

@Repository
public interface LessonRepository extends JpaRepository<Lesson,Long> {
}
