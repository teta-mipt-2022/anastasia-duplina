package spring.list.demo.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import spring.list.demo.model.Course;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {

	@Query("select c.id,count (l.id)from Course c "+
			"left join Lesson  l on c.id=l.couresId " +
			"group by c.id " +
			"order by c.id")
	List<?> findAllCoursesAndCountOfLessons();

	List<Course> findAllByUserId(@NotNull Integer userId);

	@Query("select c from Course  c " +
			"where c.date> :DateFrom " +
			"and c.date< :DateTo")
	List<Course> findFromDateToDate(@Param("DateFrom") LocalDate DateFrom, @Param("DateTo")LocalDate DateTo);

}