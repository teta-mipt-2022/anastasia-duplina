package spring.list.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import spring.list.demo.model.User;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {

	@Query("from User u " +
			"where u.id not in ( " +
			"select u.id " +
			"from User u " +
			"left join u.courses c " +
			"where c.id = :courseId)")
	List<User> findUsersNotAssignedToCourse(@Param("courseId") long courseId);
}
