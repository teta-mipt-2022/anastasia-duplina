package spring.list.demo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import spring.list.demo.model.Course;
import spring.list.demo.model.Lesson;
import spring.list.demo.model.User;
import spring.list.demo.repository.CourseRepository;
import spring.list.demo.repository.LessonRepository;
import spring.list.demo.repository.UserRepository;

import java.util.List;

@SpringBootTest
class DemoApplicationTests {

}
