package org.example.request;

import lombok.Data;
import org.springframework.lang.Nullable;

@Data //такой же, как requestV1
public class RequestV2 {
	@Nullable
	String likeString;  //like-строка
}
