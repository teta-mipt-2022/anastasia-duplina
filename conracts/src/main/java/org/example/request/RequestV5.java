package org.example.request;

import lombok.Data;
import org.springframework.lang.Nullable;

@Data
public class RequestV5 {
	@Nullable
	String likeString;  //like-строка
	@Nullable
	String regexpString;  //regexp-строка
	@Nullable
	String lastName;
	@Nullable
	Long ageFrom;
	@Nullable
	Long ageTo;
}
