package org.example.request;

import lombok.Data;
import org.springframework.lang.Nullable;

@Data
public class RequestV4 {
	@Nullable
	String likeString;  //like-строка
	@Nullable
	String regexpString;  //regexp-строка
}
