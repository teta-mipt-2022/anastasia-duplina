package org.example.response;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class User {
	String name;
	String lastName;
	@NotNull
	String login;
}
