package org.example.response;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class ResponseV2 {
	@NotNull
	List<String> logins;
	@NotNull
	Long count; //новое свойство

}
