package org.example.response;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;
@Data
public class ResponseV1 {
	@NotNull
	List<String> logins;
}
