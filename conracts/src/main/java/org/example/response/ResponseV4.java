package org.example.response;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class ResponseV4 {
	@NotNull
	@Deprecated(forRemoval = true)
	List<String> logins;
	@NotNull
	Long count;
	@NotNull
	List<User> loginsV2;


}
