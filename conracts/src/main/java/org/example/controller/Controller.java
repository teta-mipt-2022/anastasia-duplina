package org.example.controller;

import org.example.request.*;
import org.example.response.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;

@RestController
@RequestMapping("/api")
@Validated
public class Controller {
	ArrayList<String> allLogins=new ArrayList<>();
	ArrayList<User> allLoginsV2=new ArrayList<>();
	ArrayList<String> likeStrings = new ArrayList<>();
	ArrayList<String> regexpStrings=new ArrayList<>();
	ArrayList<User> likeStringsV2 = new ArrayList<>();
	ArrayList<User> regexpStringsV2=new ArrayList<>();
	@GetMapping("/getUsers/v1")
	public ResponseV1 getUsersV1(@RequestBody @Valid RequestV1 requestV1){
		//ArrayList<String> allLogins=new ArrayList<>();
		ResponseV1 responseV1=new ResponseV1();
		if(requestV1.getLikeString()==null){
			responseV1.setLogins(allLogins);
		}else{
			responseV1.setLogins(likeStrings);//some list of logins
		}
		return responseV1;
	}
	@GetMapping("/getUsers/v2")
	public ResponseV2 getUsersV2(@RequestBody @Valid RequestV2 requestV2){
		ResponseV2 responseV2 = new ResponseV2();
		if(requestV2.getLikeString()==null){
			responseV2.setLogins(allLogins);
		}else{
			responseV2.setLogins(likeStrings);
		}
		responseV2.setCount(30L);
		return responseV2;
	}
	@GetMapping("/getUsers/v3")
	public ResponseV3 getUsersV3(@RequestBody @Valid RequestV3 requestV3){
		ResponseV3 responseV3=new ResponseV3();
		if(requestV3.getLikeString()==null){
			responseV3.setLogins(allLogins);
			responseV3.setLoginsV2(allLoginsV2);
		}else{
			responseV3.setLogins(likeStrings);
			responseV3.setLoginsV2(likeStringsV2);
		}
		responseV3.setCount(30L);
		return responseV3;
	}
	@GetMapping("/getUsers/v4")
	public ResponseV4 getUsersV4(@RequestBody @Valid RequestV4 requestV4)
	{
		ResponseV4 responseV4=new ResponseV4();
		if(requestV4.getLikeString()==null && requestV4.getRegexpString()==null){
			responseV4.setLogins(allLogins);
			responseV4.setLoginsV2(allLoginsV2);
		}else{
			if (requestV4.getRegexpString() == null) {
				responseV4.setLogins(likeStrings);// результат от поиска по like строке
				responseV4.setLoginsV2(likeStringsV2);
			}else{
				responseV4.setLogins(regexpStrings);// результат от поиска по regexp строке
				responseV4.setLoginsV2(regexpStringsV2);
			}
		}
		responseV4.setCount(30L);
		return responseV4;
	}
	@GetMapping("/getUsers/v5")
	public ResponseV5 getUsersV5(@RequestBody @Valid RequestV5 requestV5){
		ArrayList<String> responseList=new ArrayList<>();
		ArrayList<User> responseListV2=new ArrayList<>();
		ResponseV5 responseV5=new ResponseV5();
		if(requestV5.getLikeString()==null && requestV5.getRegexpString()==null && requestV5.getLastName()==null
			&& requestV5.getAgeTo()==null && requestV5.getAgeFrom()==null){
			responseV5.setLogins(allLogins);
			responseV5.setLoginsV2(allLoginsV2);
		}else {
			if(requestV5.getRegexpString()==null){
				responseList=likeStrings;// результат от поиска по like строке
				responseListV2=likeStringsV2;
			}else {
				responseList=regexpStrings;
				responseListV2=regexpStringsV2;
			}
			if(requestV5.getLastName()!=null){
				//выборка по фамилии, запись в responseList и в responseListV2
				// пересечение результата с уже записанными данными
			}
			if(requestV5.getAgeFrom()!=null){
				//выборка по возрасту снизу, запись в responseList и в responseListV2
				// пересечение результата с уже записанными данными
			}
			if(requestV5.getAgeTo()!=null){
				//выборка по возрасту сверху, запись в responseList и в responseListV2
				// пересечение результата с уже записанными данными
			}
			responseV5.setLogins(responseList);
			responseV5.setLoginsV2(responseListV2);
		}
		responseV5.setCount(30L);
		return responseV5;
	}
}
